# Dijsktra's mini language interpreter
## Overview
The project's goal is to build an interpreter for Dijkstra's mini language. Refer a [Dijkstra's paper](http://adamant.kaist.ac.kr/cs322/etc/Dijk_Guarded%20Commands,%20Nondeterminancy%20and%20Formal%20Derivation%20of%20Programs.pdf) which describes the language. Hereinafter, the project's implementation of the language will be denoted by Minilang.

The project is written in JavaScript and requires [Node.js](https://nodejs.org/en/) to run.
The parser internally uses using [Jison](https://zaa.ch/jison/), a variant of Lexx/Yacc or Flex/Bison, for lexical analysis and parsing.

## Setup

1. Install Node.js v8.11.1, LTS version [Node.js homepage](https://nodejs.org/en/)
2. Download the project (e.g., `git clone git@gitlab.com:wonjung/minilang-interp.git`)
3. Install Jison: Enter `npm install` in the project's root directory.

## How to run 
### AST Generation


Code usage:
```javascript
const Parser = require("./parser");
let input = "x := 6; z:= x + 10;" // minilang source code
let ast = Parser.parse(input) // call .parse
let output = JSON.stringify(ast, null, 2) // convert JSON object to string
console.log(output);
```

"examples" directory contains example minilang program files. Executing parse_example.js (`node parse_examples.js`) will read the examples and generate AST files under "examples/ast" directory.

### Interpretation
Code usage:
```javascript
const Interpreter = require("./parser");
let code = "x := 6; z:= x + 10;y := x + z;" // minilang source code
let input = {"x":10, "y":20}
let output = Interpreter.interp(code, input)
console.log(output);
```
The input is passed to a program as a JavaScript object and the program also outputs a JavaScript object. The output captures final state of an executed program.
Executing parse_example.js (`node parse_examples.js`) will read the examples and read example input arguments under "examples/input" directory.
After executing all statements, the program's final state is saved in the JSON format under "examples/output" directory.

#### Sorting 6 numbers
`interp_sort_six_numbers.js` is an example that shows correctness of the project's minilang interpreter implementation. Even the execution order of the program is non-deterministic (repetitive statements), the program always terminates and results in the correct state.

For example, six numbers '400 5 25 7 8 30' are given as input. The program sorts the six numbers and terminates `Output (sorted):  [ 5, 7, 8, 25, 30, 400 ]`
```
shell> node interp_sort_six_numbers.js 400 5 25 7 8 30
Source code: 
 w, u, v, x, y, z := W, U, V, X, Y, Z;

do w > u -> w, u := u, w;
 | u > v -> u, v := v, u; 
 | v > x -> v, x := x, v; 
 | x > y ->  x, y := y, x; 
 | y > z -> y, z := z, y; 
od
Input:  [ 400, 5, 25, 7, 8, 30 ]
Initial program state:  { X: 400, Y: 5, Z: 25, U: 7, W: 8, V: 30 }
Final program state:  { X: 400,
  Y: 5,
  Z: 25,
  U: 7,
  W: 8,
  V: 30,
  w: 5,
  u: 7,
  v: 8,
  x: 25,
  y: 30,
  z: 400 }
Output (sorted):  [ 5, 7, 8, 25, 30, 400 ]
```


## Project structure

## Minilang Grammar
### Supported data types
Minilang supports number and boolean values.
1. Number (e.g., 3. 4.0, -5)
2. Boolean (`true`, `false`)

Minilang supports basic operations related to number or boolean. Their usage is analogous to commonly used languages like C or Java.

* Arithmetic operators; addition (+), subtraction (-), multiplication (\*), division (/), negation (-)
* Comparasion operators; equal (==), less than (<), grater than (>), less than or equal to (<=), greater than or equal to (>=)
* Logical operators; and (&&), or (||), negation (!)

An experssion can be parenthesized or nested in another expression (e.g., `3 + 3 * (5 + 5)`). 

### Supported statements
Minilang consists of a sequence of statements. Statements are executed in the sequential order. Currently, following types of statements are supported. 

1. Alternative statement (e.g., if ... fi)
2. Repetitive statement (e.g., do (x < 6)->{ x := x + 1; } od)
3. (Concurrent) assignment statement (e.g., `x, y := y, x;`)
4. Skip statement (e.g., `skip;`)
5. Abort statement (e.g, `abort;`)

Semantics of the statements are described in the [Dijkstra's paper](http://adamant.kaist.ac.kr/cs322/etc/Dijk_Guarded%20Commands,%20Nondeterminancy%20and%20Formal%20Derivation%20of%20Programs.pdf).

Minilang's statements are separated by semicolons (;). Alternative statement and repetitive statement are allowed to omit a semicolon. Even without a semicolon, the keywords wrapping the statement ("if ... fi, do ... od") allows unambiguous interpretation. 

Unlike a language like Python, indentation or space does not matter in Minilang. For example, the  two codes are semantically equivalent. 
```
if (guard1) -> { statement1; }
| (guard2) -> { statment2; }
fi
```

```
if (guard1) -> { statement1; } | (guard2) -> { statment2; } fi
```

### Alternative statement (if ... fi)
An alternative statement consists of multiple guaded commands. A guarded command has a guard (boolean expression) and a command (statements). A command is executed if its corresponding guard is true. If multiple guards are true, one of their command is chosen _non-deterministically_ and executed. In the current implementation, all the candidate commands have uniform probability to be executed. Minilang requires a devleper to handle the non-determinism; The program's correctness should be not affected by the interpreter's choices. In the current implementation, all the candidate commands have uniform probability to be executed.


Minilang expects at least one guard to be true. If none of guard is true, Minilang considers it an error and halts the program's execution immediately. Namely, `if fi` is semantically equivalent to `abort`.

For instance, the following program assigns maximum of two numbers to `m`. Note that the program runs correctly in case of `x == y` where a command is chosen arbitrarily. 
```
if (x >= y) -> { m := x;}
| (x <= y) -> { m := y;}
fi 
```

The syntax for alternative statement is `if guard_1 -> { statements_2 } | ... | guard_n -> { statements_n} fi`. The whole statement is wrapped by `if fi` and guarded commands are separated by `|`. A guard and a command is separated by the right arrow, `->`. 

### Repetitive statement (do ... od)
Repetitive statements, similar to an alternative statement, consists of multiple guarded commands. The interpreter executes one of commands whose guard is true. Different from an alterantive statement, the execution is repeated. The repetition stops when all guards become false. Similar to alternative statements, all guarded commands have uniform probability to be executed. Note that `do od` is semantically equivalent to `skip;`. 


The syntax for reptetive statement is idenetical to alternative statement, except that 'do od' wraps a repetitive statement.





## Abstract Syntax Tree Represenstation
In the project, an AST is represented as a Javscript object, [json](https://www.json.org/).
The root object is an array which holds Javascripts objects.
Each object in the array corresponds to a statement of a program.

For instance, given a simple prgram, `z := 2.5 + 7.5`, it produces the following object:
```
[
  {
    "tag": "assignment",
    "varnames": [
      "z"
    ],
    "values": [
      {
        "tag": "add",
        "operands": [
          {
            "tag": "number",
            "value": 2.5
          },
          {
            "tag": "number",
            "value": 7.5
          }
        ]
      }
    ]
  }
]
```

Except the root node, all nodes are represented as an object (i.e., key-value pairs).
They always have a field `tag` that describes what a node stands for (e.g., "assignment", "add"). A node also holds its children nodes in it (e.g., "varnames" of "assignment", "operands" of "add").


## Example programs of Minilang
### Example 1: Maximum of two numbers
The following minilang program computes maximum number of two numbers, `x` and `y`.
```
if (x >= y) -> { m := x;}
| (x <= y) -> { m := y;}
fi 
```

Given the source code, the parser generates following AST:
```json
[
  {
    "tag": "alternative-statement",
    "guarded_commands": [
      {
        "guard": {
          "tag": "geq",
          "operands": [
            {
              "tag": "identifier",
              "name": "x"
            },
            {
              "tag": "identifier",
              "name": "y"
            }
          ]
        },
        "command": [
          {
            "tag": "assignment",
            "varnames": [
              "m"
            ],
            "values": [
              {
                "tag": "identifier",
                "name": "x"
              }
            ]
          }
        ]
      },
      {
        "guard": {
          "tag": "ge",
          "operands": [
            {
              "tag": "identifier",
              "name": "x"
            },
            {
              "tag": "identifier",
              "name": "y"
            }
          ]
        },
        "command": [
          {
            "tag": "assignment",
            "varnames": [
              "m"
            ],
            "values": [
              {
                "tag": "identifier",
                "name": "y"
              }
            ]
          }
        ]
      }
    ]
  }
]
```
Its pretty-printed output is:
```
{Alternative(if)
  {GuardedCommand
    {Guard
      {<=
        ID(x),
        ID(y)
      }
    }
    [Command    
      {Assign
        [Ids
          ID(m)
        ]
        [Values
          ID(x)
        ]
      }
    ]
  }
  {GuardedCommand
    {Guard
      {>
        ID(x),
        ID(y)
      }
    }
    [Command    
      {Assign
        [Ids
          ID(m)
        ]
        [Values
          ID(y)
        ]
      }
    ]
  }
}
```


### Example 2: Sorting four numbers
The following minilang program sorts five numbers, U, V, X, Y, Z:
```
u, v, x, y, z := U, V, X, Y, Z;

do u > v -> { u, v := v, u; }
 | v > x -> { v, x := x, v; }
 | x > y -> { x, y := y, x; }
 | y > z -> { y, z := z, y; }
od 
```

Given the source code, the parser generates following AST:
```
[
  {
    "tag": "assignment",
    "varnames": [
      "u",
      "v",
      "x",
      "y",
      "z"
    ],
    "values": [
      {
        "tag": "identifier",
        "name": "U"
      },
      {
        "tag": "identifier",
        "name": "V"
      },
      {
        "tag": "identifier",
        "name": "X"
      },
      {
        "tag": "identifier",
        "name": "Y"
      },
      {
        "tag": "identifier",
        "name": "Z"
      }
    ]
  },
  {
    "tag": "repetitive-statement",
    "guarded_commands": [
      {
        "guard": {
          "tag": "ge",
          "operands": [
            {
              "tag": "identifier",
              "name": "u"
            },
            {
              "tag": "identifier",
              "name": "v"
            }
          ]
        },
        "command": [
          {
            "tag": "assignment",
            "varnames": [
              "u",
              "v"
            ],
            "values": [
              {
                "tag": "identifier",
                "name": "v"
              },
              {
                "tag": "identifier",
                "name": "u"
              }
            ]
          }
        ]
      },
      {
        "guard": {
          "tag": "ge",
          "operands": [
            {
              "tag": "identifier",
              "name": "v"
            },
            {
              "tag": "identifier",
              "name": "x"
            }
          ]
        },
        "command": [
          {
            "tag": "assignment",
            "varnames": [
              "v",
              "x"
            ],
            "values": [
              {
                "tag": "identifier",
                "name": "x"
              },
              {
                "tag": "identifier",
                "name": "v"
              }
            ]
          }
        ]
      },
      {
        "guard": {
          "tag": "ge",
          "operands": [
            {
              "tag": "identifier",
              "name": "x"
            },
            {
              "tag": "identifier",
              "name": "y"
            }
          ]
        },
        "command": [
          {
            "tag": "assignment",
            "varnames": [
              "x",
              "y"
            ],
            "values": [
              {
                "tag": "identifier",
                "name": "y"
              },
              {
                "tag": "identifier",
                "name": "x"
              }
            ]
          }
        ]
      },
      {
        "guard": {
          "tag": "ge",
          "operands": [
            {
              "tag": "identifier",
              "name": "y"
            },
            {
              "tag": "identifier",
              "name": "z"
            }
          ]
        },
        "command": [
          {
            "tag": "assignment",
            "varnames": [
              "y",
              "z"
            ],
            "values": [
              {
                "tag": "identifier",
                "name": "z"
              },
              {
                "tag": "identifier",
                "name": "y"
              }
            ]
          }
        ]
      }
    ]
  }
]
```
Its pretty-printed output is:
```
{Assign
  [Ids
    ID(u),
    ID(v),
    ID(x),
    ID(y),
    ID(z)
  ]
  [Values
    ID(U),
    ID(V),
    ID(X),
    ID(Y),
    ID(Z)
  ]
}
{Repetitive(do)
  {GuardedCommand
    {Guard
      {>
        ID(u),
        ID(v)
      }
    }
    [Command    
      {Assign
        [Ids
          ID(u),
          ID(v)
        ]
        [Values
          ID(v),
          ID(u)
        ]
      }
    ]
  }
  {GuardedCommand
    {Guard
      {>
        ID(v),
        ID(x)
      }
    }
    [Command    
      {Assign
        [Ids
          ID(v),
          ID(x)
        ]
        [Values
          ID(x),
          ID(v)
        ]
      }
    ]
  }
  {GuardedCommand
    {Guard
      {>
        ID(x),
        ID(y)
      }
    }
    [Command    
      {Assign
        [Ids
          ID(x),
          ID(y)
        ]
        [Values
          ID(y),
          ID(x)
        ]
      }
    ]
  }
  {GuardedCommand
    {Guard
      {>
        ID(y),
        ID(z)
      }
    }
    [Command    
      {Assign
        [Ids
          ID(y),
          ID(z)
        ]
        [Values
          ID(z),
          ID(y)
        ]
      }
    ]
  }
}
```
