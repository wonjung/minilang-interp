const Interpreter = require("./interp");
const fs = require('fs');

fs.readdirSync("./examples").forEach(file => {
  if (!file.endsWith(".minilang")) {
    return;
  }
  console.log(`Input: ${file}`);

  let src = fs.readFileSync(`./examples/${file}`, "utf-8");
  let inputSrc = fs.readFileSync(`./examples/${file}`, "utf-8");
  //console.log("src", src);
      //console.log(src);
  let name = file.split(".")[0];
  let inputArgumentsSrc = fs.readFileSync(`./examples/input/${name}.json`, "utf-8"); 
  let input = JSON.parse(inputArgumentsSrc)
  //
  try {
    console.log("Input: ", input)
    let store = Interpreter.interp(src, input);
    let output = JSON.stringify(store, null, 2);
    fs.writeFileSync(`./examples/output/${name}.txt`, output, "utf-8");
    console.log(`Output written to ./examples/ast/${name}.json`);
    console.log("Output: ", output);
  } catch (err) {
    console.error(err);
  }

})
