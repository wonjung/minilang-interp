const Generator = require("jison").Generator;
const Parser = require("jison").Parser;
const fs = require("fs");

//https://developer.apple.com/library/content/documentation/Swift/Conceptual/Swift_Programming_Language/zzSummaryOfTheGrammar.html

exports.grammar = {
    "comment": "Dijstra's mini language",
    "author": "Wonjung Kim",
    "lex": {
       "macros": {
            "digit": "[0-9]",
            "esc": "\\\\",
            "id": "[a-zA-Z][a-zA-Z0-9]*",
            "int": "-?(?:[0-9]|[1-9][0-9]+)",
            "frac": "(?:\\.[0-9]+)"
        },
        "rules": [
            ["\\s+", "/* skip whitespace */"],
            ["[0-9]+(?:\\.[0-9]+)?\\b", "return 'NUMBER'"],
            ["\\bif\\b", "return 'IF'"],
            ["\\bfi\\b", "return 'FI'"],
            ["\\bdo\\b", "return 'DO'"],
            ["\\bod\\b", "return 'OD'"],
            ["->", "return 'RARROW'"],
            ["\\{", "return 'LBRACE'"],
            ["\\}", "return 'RBRACE'"],
            ["\\(", "return 'LPAREN'"],
            ["\\)", "return 'RPAREN'"],
            [",", "return 'COMMA'"],
            [";", "return 'SEMICOLON'"],
            ["\\*\\*", "return 'EXP'"],
            ["\\*", "return 'MULT'"],
            ["\\/", "return 'DIVIDE'"],
            [":=",  "return 'ASSIGN'"],

            ["-",  "return 'MINUS'"],
            ["\\+", "return 'PLUS'"],
            //["\\^", "return '^'"],
           
            ["==",  "return 'EQ'"],
            ["!=",  "return 'NEQ'"],
            [">=", "return 'GEQ'"],
            ["<=", "return 'LEQ'"],
            [">", "return 'GE'"],
            ["<", "return 'LE'"],
            ["!",  "return '!'"],

            //["PI\\b", "return 'PI'"],
            //["E\\b", "return 'E'"],
            ["$", "return 'EOF'"],
            ["true\\b", "return 'TRUE'"],
            ["false\\b", "return 'FALSE'"],
            ["skip\\b", "return 'SKIP'"],
            ["abort\\b", "return 'ABORT'"],
            ["&&", "return 'AND'"],
            ["\\|\\|", "return 'OR'"],
            ["\\|", "return '|'"],
            ["{id}", "return 'IDENTIFIER'"],
        ]
    },

   // "tokens": "STRING NUMBER + TRUE FALSE NULL",
    "start": "program",

    "operators": [
       ["left", "PLUS", "MINUS"],
       ["left", "MULT", "DIVIDE"],
       ["left", "OR", "AND"],
       ["left", "LE", "LEQ", "GE", "GEQ", "NEQ", "EQ"],
       ["left", "^"],
       ["right", "!"],
       ["left", "UMINUS"]
    ],

    "bnf": {
       "program": [
           ["statements EOF", "return $1"],
       ],
        "statements": [
            ["statement statements", "$$=$2; $2.unshift($1);"],
            ["", "$$ = []"]
        ],
       "statement": [
            ["assignment-statement SEMICOLON", "$$=$1"],
            ["alternative-statement", "$$=$1"],
            ["repetitive-statement", "$$=$1"],
            ["skip-statement SEMICOLON", "$$=$1"],
            ["abort-statement SEMICOLON", "$$=$1"],
            ["SEMICOLON", "$$={'tag': 'noop'}"]
        ],
        "assignment-statement": [
            ["IDENTIFIER ASSIGN expr", "$$={'tag': 'assignment', 'varnames': [$1], 'values':[$3]}"],  
            ["IDENTIFIER COMMA assignment-statement COMMA expr", 
                "$$=$3;  $3['varnames'].unshift($1); $3['values'].push($5);"]
        ],
        "alternative-statement": [
            ["IF guarded-command-set FI", 
             "$$={'tag': 'alternative-statement', 'guarded_commands': $2}"],
            ["IF FI", 
             "$$={'tag': 'alternative-statement', 'guarded_commands': []}"],
        ],
        "guarded-command-set":[
            ["guarded-command-set | guarded-command", "$$=$1; $1.push($3)"],
            ["guarded-command", "$$=[$1]"],
        ],
        "guarded-command": [
            ["expr RARROW statement", "$$={'tag':'guarded-command', 'guard':$1, 'command':[$3]}"],
            ["expr RARROW LBRACE statements RBRACE", "$$={'tag':'guarded-command', 'guard':$1, 'command':$4}"]    
        ],
        "boolean-literal":[
            ["TRUE", "$$={'tag':'bool', 'value':true, 'lineno':@1.first_line, 'col_offset':@1.first_column}"],
            ["FALSE", "$$={'tag':'bool', 'value':false, 'lineno':@1.first_line, 'col_offset':@1.first_column}"],
        ],
        "repetitive-statement": [
            ["DO guarded-command-set OD", 
             "$$={'tag': 'repetitive-statement', 'guarded_commands': $2}"],
            ["DO OD", 
             "$$={'tag': 'repetitive-statement', 'guarded_commands': []}"],
        ],
        "skip-statement": [
            ["SKIP", "$$={'tag':'skip'}"],
        ],
        "abort-statement": [
            ["ABORT", "$$={'tag':'abort'}"],
        ],
       "expr": [
            ["expr PLUS expr",  "$$ = {'tag':'add', operands:[$1, $3]}"],
            ["expr MINUS expr",  "$$ = {'tag':'subtraction', operands:[$1, $3]}"],
            ["expr MULT expr",  "$$ = {'tag':'multiplication', operands:[$1, $3]}"],
            ["expr DIVIDE expr",  "$$ = {'tag':'division', operands:[$1, $3]}"],
            ["expr AND expr", "$$ = {'tag':'and', operands:[$1, $3]}"],
            ["expr OR expr", "$$ = {'tag':'or', operands:[$1, $3]}"],
            ["expr EQ expr", "$$ = {'tag':'eq', operands:[$1, $3]}"],
            ["expr LE expr", "$$ = {'tag':'le', operands:[$1, $3]}"],
            ["expr GEQ expr", "$$ = {'tag':'geq', operands:[$1, $3]}"],
            ["expr GE expr", "$$ = {'tag':'ge', operands:[$1, $3]}"],
            ["expr LEQ expr", "$$ = {'tag':'ge', operands:[$1, $3]}"],
            ["! expr", "$$ = {'tag':'negation-bool', operand:$2}"],
            //["expr ^ expr",  "$$ = {'tag':'pow', operands:[$1, $3]}"],
            ["MINUS expr",    "$$ = {'tag':'negation-number', operand:$2}", {"prec": "UMINUS"}],
            ["LPAREN expr RPAREN",  "$$ = $2"],
            ["NUMBER", "$$ = {'tag':'number', 'value':Number($1), 'lineno':@1.first_line, 'col_offset':@1.first_column};"],
            ["boolean-literal", "$$ = $1"],
            ["IDENTIFIER", "$$ = {'tag':'identifier', 'name':$1}; "],
            //["E",      "$$ = Math.E"],
            //["PI",     "$$ = Math.PI"]
        ],
    }
};

// var options = {type: "slr", moduleType: "commonjs", moduleName: "jsonparse"};

exports.main = function main (args) {
    //var code = new Generator(exports.grammar, options).generate();
    console.log("input: ", args[2])
    var parser = new Parser(exports.grammar);

    // generate source, ready to be written to disk
    var parserSource = parser.generate();
    let text = fs.readFileSync(args[2], "utf-8");
    console.log("text", text);
    var result = parser.parse(text);
   
    console.log("result: ", JSON.stringify(result, null, 2));

};

exports.parse = function parse(src) {
    var parser = new Parser(exports.grammar);
    return parser.parse(src);
}



exports.prettyOutput = (ast)=>{
    let step = 2;
    let output = ""
    let printStmt = (stmt, indent) =>{
        const base = " ".repeat(indent);
        let tag = stmt.tag;
        let output = "";
        let nodename = {
            "alternative-statement": "Alternative(if)",
            "repetitive-statement": "Repetitive(do)",
            "negation-number": "NumNegation(!)",
            "negation-bool": "BoolNegation(~)",
            "skip":"SKIP",
            "abort":"ABORT"
        }[tag];
        switch (tag) {
            case "assignment":
                let vars = stmt["varnames"];
                let vals = stmt["values"]
                output += `${base}{Assign\n`;
                output += `${base}  [Ids\n`;
                output += vars.map(varname=>`${base}    ID(${varname})`).join(",\n");
                if (vars.length > 0) {
                    output += "\n";
                }
                output += `${base}  ]\n`;
                output += base + `  [Values\n`
                output += vals.map(val=>printStmt(val, indent + step * 2)).join(",\n");
                if (vals.length > 0) {
                    output += "\n";
                }
                output += `${base}  ]\n`
                output += `${base}}`;

                break;
            case "alternative-statement":
            case "repetitive-statement":

                let guardedCommands = stmt["guarded_commands"];
                output += base + `{${nodename}`;
                if (guardedCommands.length > 1) {
                  output += "\n";
                  for (let gc of guardedCommands) {
                     output += printStmt(gc, indent + step) + "\n";
                  }    
                }


                output += `${base}}`;
                break;
            case "guarded-command":
                let guard = stmt["guard"];
                let command = stmt["command"];
                output += base + "{GuardedCommand\n"
                output += base + "  {Guard\n"
                output += printStmt(guard, indent + 2 * step) + "\n";
                output += base + "  }\n"
                output += base + "  [Command"
                if (command.length > 0) {
                    output += base + "  \n";
                    for (let c of command) {
                      output +=  printStmt(c, indent + 2 * step)+"\n";
                    }
                } 
                
                output += base + "  ]\n";
                output += `${base}}`
                break;
            case "bool":
                output += base + `Bool(${stmt.value})`;
                break;
            case "number":
                output += base + `Num(${stmt.value})`;
                break;
            case "add":
            case "subtraction":
            case "multiplication":
            case "division":
            case "and":
            case "or":
            case "eq":
            case "le":
            case "ge":
            case "leq":
            case "geq":
                let symbol = {
                    "add": "+", "subtraction": "-", "division": "/",
                    "multiplication": "*",
                    "and": "AND", "or": "OR", "eq": "==",
                    "le": "<", "ge": ">", "leq": "<=", "geq": "<=" 
                }[stmt.tag];
                let operands = stmt["operands"];
                output += base + `{${symbol}\n`;
                output += `${printStmt(operands[0], indent +  step)},\n`;
                output += `${printStmt(operands[1], indent +  step)}\n`;
                output += base + "}";
                break;
            case "negation-number":
            case "negation-bool":

                output += `${base}{${nodename}\n`;
                output += printStmt(stmt.operand, indent + step) +"\n";
                output += base + "}";
                break;
            case "identifier":
                output = `${base}ID(${stmt.name})`;
                break;
            case "skip":
            case "abort":
                output += `${base}${tag}`;
            case "noop":
                break;
            default:
                console.error("unhandled tag: ", tag);
                throw "unhandled: " + tag;
        }
        return output;
    }
    var astOutput = "";
    for (let stmt of ast) {
        astOutput += printStmt(stmt, 0) + "\n";
    }
    return astOutput;
}

//if (require.main === module)
//    exports.main(process.argv);
