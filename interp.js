const Parser = require("./parser");
const fs = require('fs');
const assert = require('assert');


exports.interp = function(src_txt, _initial_store={}) {
	let initial_store = {}
	Object.keys(_initial_store)
		.forEach((k)=>{
			let val = _initial_store[k]
			if (typeof(val) === "number") {
				initial_store[k] = {tag:"number", value:val}
			} else if (typeof(val) === "boolean") {
				initial_store[k] = {tag:"boolean", value:val}
			}
			
		})
	let ast = Parser.parse(src_txt);
	//console.log(ast);
	let output = interp_ast(ast, initial_store);

	let convertOutput = (_output)=>{
		let output = {}
		Object.keys(_output)
			.forEach((k)=>{
				let val = _output[k]
				output[k] = val.value
			})
		return output
	}
	return convertOutput(output);
}
//let errmsg = `(line: ${left.lineno}, col: ${left.col_offset}) NameError: name '${left.name}' is not defined`
function interp_ast(ast, initial_store) {
	let get_value_or_error = (identifier, store)=>{
		assert(identifier.tag == "identifier");
		let name = identifier.name;
		if (name in store) {
			return store[name];
		} else {
			return {tag: "error", type: "NameError", identifier: identifier};
		}
	}
	let typecheck = (node, expected_type)=> {
		// Return 'null' if it's okay. otherwise, return TypeError.
		if (node.tag !== expected_type) {
			if (node.tag == "error") return node;
			return {tag:"error", type:"TypeError", expected_type: expected_type, node: node};
		}
		return null;
	}

	let eval_expr = (expr, store) => {
		// returns: {'success'}
		let tag = expr.tag;
		let num_binop_table = {
			"add": (x, y)=>x+y,
			"subtraction": (x, y)=>x-y,
			"multiplication": (x,y)=> x*y,
			"division": (x,y)=>x / y,
		}
		let num_comp_op_table = {
			"le": (x, y)=>x<y,
			"ge": (x, y)=>x>y,
			"leq": (x, y)=>x<=y,
			"geq": (x, y)=>x>=y
		}

		switch (tag) {
			case "bool":
			case "number":
				return expr;
			case "identifier":
				return get_value_or_error(expr, store);
				break;
			case "add":
			case "subtraction":
            case "multiplication":
            case "division":
           	{
				let left = eval_expr(expr.operands[0], store);
				let right = eval_expr(expr.operands[1], store);

				let left_type_ok = typecheck(left, "number");
				let right_type_ok = typecheck(right, "number");
				if (left_type_ok != null) return left_type_ok;
				if (right_type_ok != null) return right_type_ok;

				let num_binop = num_binop_table[expr.tag];
				return {"tag":"number", value:num_binop(left.value, right.value)};
			}
			break;
			case "le":
            case "ge":
            case "leq":
            case "geq":
            {
            	let left = eval_expr(expr.operands[0], store);
				let right = eval_expr(expr.operands[1], store);
				let left_type_error = typecheck(left, "number");
				let right_type_error = typecheck(right, "number");

				if (left_type_error != null) return left_type_error;
				if (right_type_error != null) return right_type_error;
				let op = num_comp_op_table[expr.tag];
				return {"tag":"bool", value:op(left.value, right.value)};
			}
			break;
			case "negation-number":
			{
				let operand = eval_expr(expr.operand, store);
				let type_error = typecheck(operand, "number");
				if (type_error != null) return type_error;
				return {"tag":"number", value:-operand.value};
			}
			break;
			case "and":
			case "or":
			case "eq":
			{
            	let left = eval_expr(expr.operands[0], store);
				let right = eval_expr(expr.operands[1], store);
				let left_type_error = typecheck(left, "bool");
				let right_type_error = typecheck(right, "bool");

				if (left_type_error != null) return left_type_error;
				if (right_type_error != null) return right_type_error;
				let op = {
					"and": (x, y)=>x&&y,
					"or": (x, y)=>x||y,
					"eq": (x, y)=>x==y,
				}[tag];
				return {"tag":"bool", value:op(left.value, right.value)};
			}
			break;
			case "negation-bool":
			{
				let operand = eval_expr(expr.operand, store);
				let type_error = typecheck(operand, "bool");
				if (type_error != null) return type_error;
				return {"tag":"bool", value:!operand.value};
			}
			break;
		}
	};

	let interp_statement = (stmt, store)=>{
		
		let tag = stmt.tag;
		//let memory = program_state.st;

		switch (tag) {
			case "noop":
			{
				return store
			}
			break;
            case "assignment":
            {
                let vars = stmt["varnames"];
                let val_exprs = stmt["values"];
                let assignment = {};
                //vars.forEach((key, i) => vars[key] = vals[i]);
                for (var i=0; i<vars.length; i+=1) {
                	let id = vars[i];
                	let val = eval_expr(val_exprs[i], store);
                	if (val.tag == "error") {
                		throw val;
                	}
                	assignment[id] = val;
                } 
                return Object.assign({}, store, assignment);

            }
            break;
            case "alternative-statement":
            {
            	let guardedCommands = stmt["guarded_commands"];
            	let candidates = guardedCommands.filter((gc)=>{
            		let cond = eval_expr(gc.guard, store);
            		let type_error = typecheck(cond, "bool");
            		if (type_error != null) {
            			throw type_error;
            		}
            		return cond.value;
            	});
            	if (candidates.length == 0) {
            		throw {tag:"error", type:"NoValidCommand"}
            	} else {
            		//let command = candidates[0].command;
            		let idx = Math.floor(Math.random()*candidates.length);
            		assert(idx < candidates.length)
            		let command = candidates[idx].command;
            		return command.reduce((acc_store, curr_stmt)=>interp_statement(curr_stmt, acc_store), store);
            	}

            }
            break;
            case "repetitive-statement":
            {
            	let guardedCommands = stmt["guarded_commands"];
            	var interim_store = Object.assign({}, store);
            	var done = false;
            	while (!done) {
            	//	console.log(interim_store);
	            	let candidates = guardedCommands.filter((gc)=>{
	            		let cond = eval_expr(gc.guard, interim_store);
	            		let type_error = typecheck(cond, "bool");
	            		
	            		if (type_error != null) {
	            			throw type_error;
	            		}
	            		return cond.value;
	            	});
	            	//console.log(candidates);
	            	if (candidates.length == 0) {
	            		done = true;
	            	} else {
	            		let idx = Math.floor(Math.random()*candidates.length)
	            		assert(idx < candidates.length)
	            		let command = candidates[idx].command;
	            		interim_store = command.reduce((acc_store, curr_stmt)=>interp_statement(curr_stmt, acc_store), interim_store);
	            	}
            	}

            	return interim_store;

            }
            break;
            case "skip":
            	return store;
            case "abort":
                throw {tag:"error", type:"AbortError"};
                //return store;
            default:
                console.error(`unhandled tag: ${tag}, statement: ${JSON.stringify(stmt)}`);
                
        }
        return output;
	};

	return ast.reduce((store, stmt)=>{
		return interp_statement(stmt, store)
	}, initial_store);
}