const Interpreter = require("./interp");
const fs = require('fs');


let src = fs.readFileSync(`./examples/sort_6items.minilang`, "utf-8");
console.log("Source code: \n", src)
let numbers = process.argv.slice(2, 8).map(x=>Number(x))
if (numbers.length != 6) {
  console.error("Exactly 6 numbers should be given (Usage: node.js interp_sort_six_numbers.js 5 20 3 1 50 3)")
  process.exit(1)
}
console.log("Input: ", numbers)
let input = {}
let varnames = ["X", "Y", "Z", "U", "W", "V"]
for (var i=0; i<numbers.length; i+=1) {
  input[varnames[i]] = numbers[i]
}
console.log("Initial program state: ", input)
let store = Interpreter.interp(src, input);
let output = [store.w, store.u, store.v, store.x, store.y, store.z]
console.log("Final program state: ", store)
console.log("Output (sorted): ", output)

