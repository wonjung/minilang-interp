const Parser = require("./parser");
const fs = require('fs');

fs.readdirSync("./examples").forEach(file => {
  if (file.endsWith(".minilang")) {
  	console.log("Input", file);
  	let src = fs.readFileSync(`./examples/${file}`, "utf-8");
  	  	//console.log(src);
  	let name = file.split(".")[0];
  	let ast = Parser.parse(src);
  	let output = JSON.stringify(ast, null, 2);
    console.log(Parser.prettyOutput(ast));
  	fs.writeFileSync(`./examples/ast/${name}.json`, output, "utf-8");
    fs.writeFileSync(`./examples/ast/${name}.txt`, Parser.prettyOutput(ast), "utf-8");
  	console.log(`Output written to ./examples/ast/${name}.json`);
  }
})
console.log("AST files are generated without an error");