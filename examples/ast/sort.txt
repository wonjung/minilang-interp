{Assign
  [Ids
    ID(u),
    ID(v),
    ID(x),
    ID(y),
    ID(z)
  ]
  [Values
    ID(U),
    ID(V),
    ID(X),
    ID(Y),
    ID(Z)
  ]
}
{Repetitive(do)
  {GuardedCommand
    {Guard
      {>
        ID(u),
        ID(v)
      }
    }
    [Command    
      {Assign
        [Ids
          ID(u),
          ID(v)
        ]
        [Values
          ID(v),
          ID(u)
        ]
      }
    ]
  }
  {GuardedCommand
    {Guard
      {>
        ID(v),
        ID(x)
      }
    }
    [Command    
      {Assign
        [Ids
          ID(v),
          ID(x)
        ]
        [Values
          ID(x),
          ID(v)
        ]
      }
    ]
  }
  {GuardedCommand
    {Guard
      {>
        ID(x),
        ID(y)
      }
    }
    [Command    
      {Assign
        [Ids
          ID(x),
          ID(y)
        ]
        [Values
          ID(y),
          ID(x)
        ]
      }
    ]
  }
  {GuardedCommand
    {Guard
      {>
        ID(y),
        ID(z)
      }
    }
    [Command    
      {Assign
        [Ids
          ID(y),
          ID(z)
        ]
        [Values
          ID(z),
          ID(y)
        ]
      }
    ]
  }
}
